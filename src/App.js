import { Outlet } from "react-router-dom";

import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from "./components/Footer";


function App() {
    
  return (
    <div className="App">

      <div className='app-header'>
        <Header />
      </div>

      <div className='app-content'>
        <Outlet/>        
      </div>

    </div>
  );
}

export default App;
