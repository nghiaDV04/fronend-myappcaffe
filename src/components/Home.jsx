import React from 'react'
import Banner from './Banner'
import CaffeList from './CaffeList'
import SaleOff from './SaleOff'
import Footer from './Footer'
import Comment from './Comment'

import { useState } from 'react';
import cafeData from '../service/CaffeData';

export default function Home() {
    const [cafes, setCafes] = useState(cafeData);

  return (
        <div id="wrapper">

            <Banner />
            <CaffeList 
               cafes={cafes}
            />
            <SaleOff />
            <Comment />
            <Footer />
                     
        </div>

        
  )
}
