import React, { useState, useEffect } from 'react'
import Footer from './Footer'


export default function Carts() {
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    // Lấy danh sách sản phẩm từ LocalStorage khi component được render
    const cartData = JSON.parse(localStorage.getItem('cart')) || [];
    setCartItems(cartData);
  }, []);

  const handleResetCart = () => {
    localStorage.removeItem("cart");
    setCartItems([]); // Cập nhật giỏ hàng trong state thành rỗng để hiển thị giỏ hàng trống trên giao diện.
  };

// Tính tổng tiền mua sản phẩm trong giỏ hàng
const getTotalPrice = () => {
  let total = 0;
  for (const item of cartItems) {
    total += item.price * item.amount;
  }
  return total;
};


const handleClickUp = (index) =>{
  const updatedCart = [... cartItems];
  updatedCart[index].amount +=1;
  setCartItems(updatedCart);

  localStorage.setItem('cart', JSON.stringify(updatedCart));

}

const handleClickDown =(index) =>{
  const updateCart =[...cartItems];
  if(updateCart[index].amount > 0){
    updateCart[index].amount -=1;
    setCartItems(updateCart);
    localStorage.setItem('cart', JSON.stringify(updateCart));
  }
}

const handleRemoveItem =(index) =>{
  const updateCart =[...cartItems];
  updateCart.splice(index, 1); // Loại bỏ phần tử tại vị trí index
  setCartItems(updateCart);
  localStorage.setItem('cart', JSON.stringify(updateCart));
}

const handleClickPay = () =>{
  const totalPrice = getTotalPrice(); 
  const totalItem = cartItems.reduce((total, item) => total + item.amount, 0);
  alert(`Cám ơn bạn đã đặt: ${totalItem} sản phẩm!!! \nTổng tiền là: $${totalPrice} `);
}

  return (
    <div>

      <section className='cart'>

      <ul>
      {cartItems.map((product, index) => (
        <li key={index}>
          <article className="cart-item">
            <img src={product.img} alt="" />
            <div>
              <h3 className='aaa'>{product.title}</h3>
              <h4 className="item-price"> ${product.price}</h4>
              <button className="remove-btn" onClick={()=>handleRemoveItem(index)}> remove </button>
            </div>

            <div>
              <button className="amount-btn" onClick={() =>handleClickUp(index)}>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path d="M10.707 7.05L10 6.343 4.343 12l1.414 1.414L10 9.172l4.243 4.242L15.657 12z" />
                </svg>
              </button>

              <p className="amount">{product.amount}</p>

              <button className="amount-btn" onClick={() => handleClickDown(index)} >
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                </svg>
              </button>
            </div>
          </article>
        </li>
      ))}
      </ul>



        <footer>
          <hr />
          <div className="cart-total">
            <h4> <span>Tổng tiền: ${getTotalPrice()}</span> </h4>
          </div>
          <button className="clear-btn" onClick={()=>handleResetCart()}> clear cart </button>
          <div>
            <button className="clear-btn" onClick={()=>handleClickPay()}> Pay </button>
          </div>

        </footer>
      </section>

      <div className='app-content'>
        <Footer />     
      </div>

    </div>
  )
}
