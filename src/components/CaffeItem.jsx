import React from 'react'

export default function CaffeItem(props) {
    const { cafeItem } = props;

    return (


        <div class="item">
            <img
                src={cafeItem.img}
                alt=""
                style={{ width: '256px', height: '174px' }}
            />
            <div class="stars">
                <span>
                    <img src="assets/star.png" alt="" />
                </span>
                <span>
                    <img src="assets/star.png" alt="" />
                </span>
                <span>
                    <img src="assets/star.png" alt="" />
                </span>
                <span>
                    <img src="assets/star.png" alt="" />
                </span>
                <span>
                    <img src="assets/star.png" alt="" />
                </span>
            </div>

            <div class="name">{cafeItem.title}</div>
            <div class="desc">{cafeItem.body}</div>
            {/* <div class="price">${cafeItem.price}</div> */}
        </div>




    )
}
