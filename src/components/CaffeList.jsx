import React from 'react'
import CaffeItem from './CaffeItem'

export default function CaffeList(props) {
    const { cafes } = props;

    const cafeListRender = cafes.map((cafeItem) => {

        return (
            <CaffeItem
                cafeItem={cafeItem}
                key={cafeItem.id} 
            />
        )
    })

    return (
        <div id="wp-products">
            <h2>SẢN PHẨM NỔI BẬT</h2>

            <ul id="list-products">
                {cafeListRender}
            </ul>
        </div>
    )
}
