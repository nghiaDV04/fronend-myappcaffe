import React from 'react'

export default function Footer() {
    return (
        <div id="footer">
            <div class="box">
            <a href="" class="logo">
                <img src="/assets/logoFeee.png" alt="" style={{ width: '100px', height: '50px' }} />
            </a>
                <p>Cung cấp sản phẩm với chất lượng an toàn cho quý khách</p>
            </div>
            <div class="box">
                <h3>NỘI DUNG</h3>
                <ul class="quick-menu">
                    <div class="item">
                        <a href="">Trang chủ</a>
                    </div>
                    <div class="item">
                        <a href="">Sản phẩm</a>
                    </div>
                    <div class="item">
                        <a href="">Blog</a>
                    </div>
                    <div class="item">
                        <a href="">Liên hệ</a>
                    </div>
                </ul>
            </div>
            <div class="box">
                <h3>LIÊN HỆ</h3>
                <form action="">
                    <input type="text" placeholder="Địa chỉ email" />
                    <button>Nhận tin</button>
                </form>
            </div>
        </div>
    )
}
