
import React, { useEffect, useState } from 'react'
import productService from '../service/product.service';
import Footer from './Footer';


export default function Products() {

  const [productList, setProductList] = useState([]);
  const [cart, setCart] = useState([]);




  const handleAddToCart = (product) => {
    const existingCart = JSON.parse(localStorage.getItem("cart")) || [];

    // Kiểm tra xem sản phẩm đã tồn tại trong giỏ hàng chưa
    const existingProduct = existingCart.find((item) => item.id === product.id);

    if (existingProduct) {
      // Nếu sản phẩm đã tồn tại trong giỏ hàng, tăng số lượng lên 1
      existingProduct.amount += 1;
    } else {
      // Nếu sản phẩm chưa tồn tại trong giỏ hàng, thêm sản phẩm vào giỏ hàng
      product.amount = 1;
      existingCart.push(product);
    }

    // Cập nhật giỏ hàng trong LocalStorage
    localStorage.setItem("cart", JSON.stringify(existingCart));
    setCart(existingCart); // Cập nhật giỏ hàng trong state
  };



  useEffect(() => {
    productService
      .getAllProduct()
      .then((res) => {
        // console.log(res.data);
        setProductList(res.data);
      })
      .catch((error) => {
        console.log(error);
      })
  }, []);

  return (
    <div>
      <div id="wp-products">
        <h2>SẢN PHẨM CỦA CHÚNG TÔI</h2>
        <ul id="list-products">

          {productList.map((product, index) => (
            <div key={index} className="item">
              <img src={product.img} alt="" style={{ width: '256px', height: '174px' }} />
              <div className="mauchu" >{product.title}</div>
              <div class="price">{product.price}</div>
              <div className="mauchu" >{product.body}</div>
              <div>
                <button className="aaaaa" onClick={() => handleAddToCart(product)}>Add to Cart</button>
              </div>
            </div>

          ))}


        </ul>
        <div class="list-page">
          <div class="item">
            <a href="">1</a>
          </div>
          <div class="item">
            <a href="">2</a>
          </div>
          <div class="item">
            <a href="">3</a>
          </div>
          <div class="item">
            <a href="">4</a>
          </div>
        </div>
      </div>


      <div className='app-content'>
        <Footer />
      </div>
    </div>
  )
}


