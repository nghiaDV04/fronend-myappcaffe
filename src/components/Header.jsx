import React from 'react'
import { Link } from "react-router-dom";

export default function Header() {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];

    return (
        <div id="header">
            <a href="" class="logo">
                <img src="/assets/logoFeee.png" alt="" style={{ width: '100px', height: '50px' }} />
            </a>
            
            <div id="menu">
                <div class="item">
                    <Link to="/">Trang chủ</Link>
                </div>
                <div class="item">
                    <Link to="/products"
                    >
                        Sản phẩm
                    </Link>

                </div>
                <div class="item">
                    <Link to="/contact">Contact</Link>
                </div>
            </div>
            
            <div id="actions">
                <div class="item">
                    <img src="/assets/user.png" alt="" />
                </div>

                <div className="item">
                    {/* Chuyển đến trang Carts */}
                    <Link to="/carts">
                        <img src="/assets/cart.png" alt="" />
                    </Link>
                    <span className="cart-count">{cart.length}</span> {/* Số lượng sản phẩm trong giỏ hàng */}
                </div>
            </div>

        </div>
    )
}
