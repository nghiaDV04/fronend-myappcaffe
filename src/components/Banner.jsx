import React from 'react'
import { Link } from 'react-router-dom'

export default function Banner() {
 

    return (
        <div id="banner">    
            <div class="box-left" >
                <h2>
                    <span>THỨC UỐNG</span>
                    <br />
                    <span>THƯỢNG HẠNG</span>
                </h2>
                <p >Chuyên cung cấp các món ăn đảm bảo dinh dưỡng
                    hợp vệ sinh đến người dùng,phục vụ người dùng 1 cái
                    hoàn hảo nhất</p>
                <Link to="/products">
                    <button>Mua ngay</button>
                </Link>
            </div>
            <div class="box-right">
                <img src="/assets/img6.jpg" alt="" style={{ width: '95px', height: '276px' }} />
                <img src="/assets/img7.jpg" alt="" style={{ width: '95px', height: '333px' }} />
                <img src="/assets/img8.jpg" alt="" style={{ width: '95px', height: '276px' }} />
            </div>
            <div class="to-bottom">
                <a href="">
                    <img src="/assets/to_bottom.png" alt="" />
                </a>
            </div>
        </div>
    )
}
// rfc tu khoa