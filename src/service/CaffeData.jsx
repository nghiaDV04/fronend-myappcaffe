const cafeData = [
    {
        id: 1,
        title: 'Caffe Đen',
        price: 15,
        img: "assets/caffeDen.jpg",
        amount: 0,
        body: ' Caffe có vị đắng đậm đà cùng với mùi hương nồng nàn quen thuộc của cà phê sẽ đánh thức vị giác', 
        
    },

    {
        id: 2,
        title: 'Caffe Sữa ',
        price: 20,
        img: "assets/caffeSuaDa.jfif",
        amount: 0,
        body: 'Caffe có vị ngọt của sữa cùng mùi hương của caffe sẽ đánh thức vị giác của bạn',
        
    },


    {
        id: 3,
        title: 'Espresso',
        price: 50,
        img: "assets/Espresso.jpg",
        amount: 0,
        body: 'Espresso của HiPB có chút chua chút đắng cùng với hương thơm nồng nàn quyến rũ. Espresso - thức uống cần cho sự tỉnh táo',
        
    },

    {
        id: 4,
        title: 'Cappucinno',
        price: 50,
        img: "assets/Cappucinno.jpg",
        amount: 0,
        body: 'Là một loại caffe gồm caffe và bọt sữa, cùng một ít cacao rắc lên trên!' ,
        // body: 'Cà phê là một loại thức uống quen thuộc của người Việt. Vị đắng đậm đà cùng với mùi hương nồng nàn quen thuộc của cà phê sẽ đánh thức vị giác, mang đến một cảm giác sảng khoái mà tỉnh táo.',
    },

    {
        id: 5,
        title: 'Bạc Xỉu',
        price: 35,
        img: "assets/Bacxiu.jfif",
        amount: 0,
        body: 'Bạc Xỉu có vị lai nhẹ giữa caffe và sữa!',
    },

    {
        id: 6,
        title: 'Nước ép cam',
        price: 35,
        img: "assets/NuocEpCam.jfif",
        amount: 0,
        body: 'Cam ép chua chua, ngọt ngọt và giàu vitamin C',
    },

    {
        id: 7,
        title: 'Caffe Muối',
        price: 30,
        img: "assets/CaffeMuoi.jfif",
        amount: 0,
        body: 'Caffe có vị măn nhẹ của muối cùng mùi hương của caffe sẽ đánh thức vị giác của bạn!',
    },

    {
        id: 8,
        title: 'Caffe Trứng',
        price: 30,
        img: "assets/CaffeTrung.jfif",
        amount: 0,
        body: 'Caffe Trứng mới lạ có vị caffe và trứng!',
    },

    {
        id: 9,
        title: 'Nước Ép Chanh Dây',
        price: 30,
        img: "assets/NuocEpChanhDay.jpg",
        amount: 0,
        body: 'Nước chanh dây ép thơm ngon, đậm vị và sảng khoái!',
    },

    {
        id: 10,
        title: 'Bánh ngọt',
        price: 30,
        img: "assets/banh.jfif",
        amount: 0,
        body: 'Bánh ngọt có vị thanh của chanh dây!',
    },
];

export default cafeData;