import axios from "axios";

const BASE_API_URL ="http://localhost:8080/api";


class ProductService{

    getAllProduct(){
        return axios.get(BASE_API_URL + "/");
    }
}

export default new ProductService();