import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import Products from './components/Products';
import Contact from './components/Contact';
import Home from './components/Home';
import Carts from './components/Carts';

import { Provider } from 'react-redux';
import Footer from './components/Footer';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
 
    <BrowserRouter>
      <Routes >
        <Route path='/' element={<App />}>
          <Route path='products' element={<Products />} />
          <Route path='/contact' element={<Contact />} />
          <Route path='/carts' element={<Carts />} />
          <Route index element={<Home />} />
        </Route>
      </Routes>
    </BrowserRouter>
 
  // </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
